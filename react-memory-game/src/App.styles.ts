import styled from 'styled-components';

export const Wrapper = styled.div`
    margin: auto;
    text-align: center;
    font-size: xx-large;

    .MuiFormControl-root {
        display: block;
    }

    .MuiFormGroup-root {
        display: block;
    }
`;

export const StyledButton = styled.button`
    font-size: x-large;
    background-color: #38c219;
    border-radius: 5px;
    margin: auto;
`;

