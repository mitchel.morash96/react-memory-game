import { useEffect, useState } from 'react';
import { shuffleArray } from './utils';
// Components
import Board from './Board/Board';
import Infobox from './Infobox/Infobox';
// Styles
import { Wrapper, StyledButton } from './App.styles';
import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from '@material-ui/core';
// Types
export type CardType = {
  id: number;
  text: string;
}

export enum Difficulty {
  EASY = 16,   // 4x4 --  8 pairs
  MEDIUM = 36, // 6x6 -- 18 pairs
  HARD = 64,   // 8x8 -- 32 pairs
}

let FRUITVEGLIST = ['Apple', 'Kiwi', 'Mango', 'Melon', 'Peach', 'Banana', 'Grape', 'Cherry',
                 'Blackberry', 'Blueberry', 'Cantaloupe', 'Cranberry', 'Coconut', 'Fig', 'Lemon', 'Lime',
                 'Pineapple', 'Raspberry', 'Pomegranate', 'Strawberry', 'Rhubarb', 'Carrot', 'Beet', 'Celery',
                 'Potato', 'Pumpkin', 'Onion', 'Lettuce', 'Tomato', 'Turnip', 'Broccoli', 'Asparagus'
];

let map: Map<string, number> = new Map<string, number>([
    ["Easy", Difficulty.EASY],
    ["Medium", Difficulty.MEDIUM],
    ["Hard", Difficulty.HARD]
]);

const App = () => {
  const [score, setScore] = useState(0);
  const [cardList, setCardList] = useState([] as CardType[]);
  const [isPlaying, setIsPlaying] = useState(false);
  const [mistakes, setMistakes] = useState(20);
  const [diff, setDiff] = useState('Easy');
  const [targets, setTargets] = useState<Array<EventTarget & HTMLDivElement>>([]);
  const [gameOver, setGameOver] = useState(false);
  const [winner, setWinner] = useState(false);
  const [waiting, setWaiting] = useState(false); 

  useEffect(() => {
    if (targets.length === 2) {
      checkMatch();
    }
  }, [targets])

  // Handler event for start button click
  const startGame = () => {
    let cardAmount: number = 0;
    let cards: string[] = [];
    
    setIsPlaying(true);
    
    FRUITVEGLIST = shuffleArray(FRUITVEGLIST);

    let num: number = map.get(diff)!;
    cardAmount = num;
    cards = buildCardList(num);

    cards = shuffleArray(cards);

    for (var i = 0; i < cardAmount; i++) {
      let temp: CardType = ({id: i, text: cards[i]});
      setCardList((prev) => {
        return [...prev, {...temp}]
      });
    }
  };

  // Build a list of cards to display
  const buildCardList = (cardCount: number) => {
    let cards: string[] = [];
    let count: number = 0;

    for (var i = 0; i < cardCount; i+=2){
      cards[i] = FRUITVEGLIST[count];
      cards[i+1] = FRUITVEGLIST[count];
      count++;
    }
    return cards;
  };

  // Handler event for changing the difficulty
  const changeDiff = (newDiff: string) => {
    setDiff(newDiff);
  };

  // Handler event for card click 
  const cardClick = (e: React.MouseEvent<HTMLDivElement>) => {
    if ((!gameOver || !winner) && !waiting) {
      if (gameOver || winner) return;
      let target: EventTarget & HTMLDivElement = e.currentTarget;
      target.children[0].className='';

      if (targets.length !== 0) {
        if (targets[0].offsetTop !== target.offsetTop || targets[0].offsetLeft !== target.offsetLeft) {
          setTargets([...targets, target]);
        }
      } else {
        setTargets([...targets, target]);
      }
    }
  };

  // Check if selected targets are a match
  const checkMatch = async () => {

    if (targets[0].children[0].children[0].getAttribute('alt') === targets[1].children[0].children[0].getAttribute('alt')) {
      setScore(score + 1);

      if (score === (map.get(diff)!/2) - 1) {
        setWinner(true);
      }
      
      targets.forEach(card => {
        card.style.visibility='hidden';
      });
    } else {
      if (mistakes === 1) {
        setGameOver(true);
      }
      setMistakes(mistakes - 1);
      
      setWaiting(true);
      await delay(1000);
      setWaiting(false);
    }
    
    targets.forEach(card => {
      card.children[0].className='hide';
    });

    setTargets([]);
  };

  // function to delay 
  const delay = (time: number) => {
    return new Promise(resolve => setTimeout(resolve, time));
  };

  return (
    <Wrapper>
      
      <h1>Memory Game!</h1>
      {isPlaying ? <Infobox score={score} mistakes={mistakes} /> : null}

      {!isPlaying ? 
        <FormControl>
          <FormLabel>Difficulty</FormLabel>
          <RadioGroup row={true} defaultValue='Easy'>
            <FormControlLabel value='Easy' control={<Radio />} label='Easy' onClick={() => changeDiff('Easy')} />
            <FormControlLabel value='Medium' control={<Radio />} label='Medium' onClick={() => changeDiff('Medium')} />
            <FormControlLabel value='Hard' control={<Radio />} label='Hard' onClick={() => changeDiff('Hard')} />
          </RadioGroup>
        </FormControl>
      : null}

      {gameOver ? <div>Game Over</div> : null}
      {winner ? <div>Winner!</div> : null}

      {!isPlaying ? 
        <StyledButton onClick={() => startGame()}>
          Start
        </StyledButton>
      : <Board cardlist={cardList} diff={diff} callback={cardClick} />}   

    </Wrapper>
  );
}

export default App;
