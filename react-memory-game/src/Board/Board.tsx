import React from 'react';
// Components
import Card from '../Card/Card';
// Styles
import Grid from '@material-ui/core/Grid';
import { Wrapper } from './Borad.styles';
// Types
import { CardType } from '../App';

type Props = {
    cardlist: CardType[];
    diff: string;
    callback: (e: React.MouseEvent<HTMLImageElement>) => void;
}

let map: Map<string, string> = new Map<string, string>([
    ["Easy", "296px"],
    ["Medium", "444px"],
    ["Hard", "592px"]
]);

const Board: React.FC<Props> = ({ cardlist, diff, callback }) => {   

    return(
        <Wrapper theme={map.get(diff)}>
            <Grid container>
                {cardlist.map((item => (
                    <Card key={item.id} text={item.text} onclick={callback} />
                )))}     
            </Grid>
        </Wrapper>
    );
}

export default Board;