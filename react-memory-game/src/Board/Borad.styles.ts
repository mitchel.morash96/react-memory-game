import styled from "styled-components";

// easy   : width: 296px;
// medium : width: 444px;
// hard   : width: 592px;

export const Wrapper = styled.div`
    width: ${props => props.theme};
    background-color: #6c0675;
    border: 3px solid black;
    margin: auto;
    text-align: center;
    line-height: 400px;
    border-radius: 10px;
`;

