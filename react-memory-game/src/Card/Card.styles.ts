import styled from "styled-components";

export const Wrapper = styled.div`
    height: 50px;
    width: 50px;
    background-color: #8f3d1d;
    border: 3px solid black;
    margin: 9px;
    line-height: 25px;
    border-radius: 5px;
    font-size: medium;

    .hide {
        content-visibility: hidden;
        color: #8f3d1d;
        word-wrap: break-word;
    }
`;
