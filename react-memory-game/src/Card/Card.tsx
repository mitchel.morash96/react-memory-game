import { Wrapper } from "./Card.styles";

type Props = {
    text: string;
    onclick: (e: React.MouseEvent<HTMLImageElement>) => void;
}

const Card: React.FC<Props> = ({ text, onclick }) => {
    
    return (
        <Wrapper onClick={onclick}>
            <div className="hide">
                <img src={require("../images/" + text + ".jpg")} alt={text} />
            </div>
        </Wrapper>
    )
}

export default Card;