import { Wrapper } from './Infobox.styles';

type Props = {
    score: number;
    mistakes: number;
}

const Infobox: React.FC<Props> = ({ score, mistakes }) => {

    return (
        <Wrapper>
            <p>Score: {score}</p>
            <p>Mistakes Left: {mistakes}</p>
        </Wrapper>
    )
}

export default Infobox